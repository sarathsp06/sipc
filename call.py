from pjsua import CallCallback as pjCallCallback
from pjsua import CallState as pjCallState
from pjsua import MediaState as pjMediaState
from constants import CallState
from lib import Lib
import utils 
from events import CallEvent
class CallCallback(pjCallCallback):
    def __init__(self, call, logger, account, notification_cb = utils.default_notifications_cb):
        pjCallCallback.__init__(self, call)
        self.logger = logger
        self.notification_cb = notification_cb
        self.account = account
        self._player_id = None
    
    def set_player_id(self, player_id):
        if self._player_id is not None:
            _stop_call_sound(self._player_id)
        self._player_id = player_id

    def on_state(self):
        call_info = self.call.info()
        event_data = {'remote_uri' :call_info.remote_uri,
                'state_text':call_info.state_text,
                'last_code':call_info.last_code,
                'last_reason':call_info.last_reason,
                'call_state': CallState[self.call.info().state]}
        if self.call.info().state == pjCallState.DISCONNECTED:
            self.notification_cb(CallEvent,CallEvent.CALL_HUNGUP, event_data)
            self.account.current_call = None            
            _stop_call_sound(self._player_id)            
        elif self.call.info().state == pjCallState.CALLING:
            self.notification_cb(CallEvent, CallEvent.CALL_DIALING, event_data)            
        elif self.call.info().state == pjCallState.CONFIRMED:
            self.notification_cb(CallEvent, CallEvent.CALL_ACTIVE, event_data)
            _stop_call_sound(self._player_id)
            transmit_audio(self.call, self.account.volume)
            
    def on_media_state(self):
        call_info = self.call.info()
        event_data = {'remote_uri' :call_info.remote_uri,
                'state_text':call_info.state_text,
                'last_code':call_info.last_code,
                'last_reason':call_info.last_reason,
                'media_state': CallState[self.call.info().media_state]}
        if call_info.media_state == pjMediaState.ACTIVE:
            _stop_call_sound(self._player_id)
            transmit_audio(self.call,self.account.volume)
            self.notification_cb(CallEvent, CallEvent.MEDIA_STATE_ACTIVE, event_data)
        elif self.call.info().media_state == pjMediaState.LOCAL_HOLD:
            self.account.call_state = "HOLD"  
            self.notification_cb(CallEvent, CallEvent.MEDIA_STATE_HOLD, event_data)
        elif self.call.info().media_state == pjMediaState.REMOTE_HOLD:
            self.account.call_state = "HOLD"
            self.notification_cb(CallEvent, CallEvent.MEDIA_STATE_HOLD, event_data)
        else:
            self.account.call_state = "IDLE"
            self.notification_cb(CallEvent, CallEvent.MEDIA_STATE_IDLE, event_data)

def transmit_audio(call, volume, record = True):
        lib = Lib.instance()
        #get the call slot
        call_slot = call.info().conf_slot
        #create recorder and generate recorder id
        recorder_id = lib.create_recorder(utils.get_record_file(call.info().sip_call_id))
        recorder_slot = None
        if recorder_id is not None:
            recorder_slot = lib.recorder_get_slot(recorder_id)
        #record if possible
        if recorder_slot is not None:
            lib.conf_connect(call_slot, recorder_slot)
        lib.conf_connect(call_slot, 0)
        lib.conf_connect(0, call_slot)
        lib.conf_set_rx_level(call_slot, volume)
        lib.conf_set_tx_level(call_slot, volume)


def transmit_recording(call, audio_file, volume):
        lib = Lib.instance()
        #get the call slot
        call_slot = call.info().conf_slot
        player_id = lib.create_player(utils.get_audio_wav(audio_file))
        lib.conf_disconnect(lib.player_get_slot(player_id), call_slot)
        lib.conf_set_rx_level(call_slot, volume)
        lib.conf_set_tx_level(call_slot, volume)
        return player_id


def start_call_sound_out(volume = 1.0):
    """
    starts the ring tone and retuns the player id
    """
    return _start_call_sound(utils.get_caller_tone(), volume)

def start_call_sound_in(volume = 1.0):
    """
    starts the caller tone and retuns the player id
    """
    return _start_call_sound(utils.get_ring_tone(), volume)


def _stop_call_sound(player_id):
    lib = Lib.instance()
    if player_id != None:
        lib.player_set_pos(player_id, 0)
        lib.conf_disconnect(lib.player_get_slot(player_id), 0)

def _start_call_sound(sound_file, volume = 1.0):
    lib = Lib.instance()
    player_id = lib.create_player(sound_file, loop=True)
    lib.conf_set_tx_level(player_id, volume)
    lib.conf_connect(lib.player_get_slot(player_id), 0)
    return player_id
