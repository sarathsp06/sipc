#!/usr/bin/python
from os import path
import logging
sounds_dir = path.join(path.dirname(path.abspath(__file__)), 'sounds')
notification_time_format = "[%Y/%m/%d %H:%M:%S]"
LOG_LEVEL = 0
LOG_LEVEL_PY = logging.DEBUG #DEBUG
MAX_CALLS = 100

class MediaDefaults:
    VOLUME = 1.0
    JITTER_BUFFER_SIZE = 1000 #millisecods
    AUDIO_QUALITY = 10 #1- 10

