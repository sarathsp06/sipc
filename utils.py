#!/usr/bin/python
from os import path
from settings import sounds_dir
import logging
import settings
from functools import wraps


def notification_wrapper(func):
    """
    notification_wrapper
    It wrapps a notificatgion function and prints the event_type,
    name  and data and does the functions functionality
    """
    @wraps(func)
    def _f(event_type, event_name, data):
        getLogger().debug("EVENT : {0},NAME:{1},DATA:{2}".format(event_type, event_name, data))
        return func(event_type, event_name, data)
    return _f

@notification_wrapper
def default_notifications_cb(event_type, event_name, data):
    """
    Default notification  callback that does nothing
    event_type : the type of event
    event_name : the event
    data       : A dictionary showing the data associated with event
    """
    pass

def get_audio_wav(filename):
    #function to get the path to wav file
    return path.join(sounds_dir, 'read', filename)

def get_ring_tone():
    return get_audio_wav("ring_tone.wav")

def get_caller_tone():
    return get_audio_wav("caller_tone.wav")

def get_record_file(id):
    return path.join(sounds_dir, 'recording', "{0}.wav".format(id))

logger_dict = {}
def getLogger(level = settings.LOG_LEVEL_PY, package = "sipc"):
    if logger_dict.get((package, level), None) is None:
        logging.basicConfig(level = level, format='[%(levelname)5s] %(asctime)-15s [%(funcName)s@%(thread)9s] : %(message)s')
        logger = logging.getLogger(package)
        logger.setLevel(level)
        logger_dict[(package, level)] = logger
    return logger_dict.get((package, level), logging.getLogger())

#not used
def withlib(func):
    @wraps(func)
    def f(*argv, **argc):
        from lib import Lib
        from constants import ErrorString
        global lib
        lib = Lib.instance()
        if lib is None:
           getLogger.error(ErrorString.UNINITIALIZED_LIB)
           return
        func(*argv, **argc)
