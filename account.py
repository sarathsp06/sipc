#!/usr/bin/python
from pjsua import AccountCallback as pjAccountCallback
from pjsua import Error as pjError
from pjsua import AccountConfig
from constants import StatusCode, AccountState, CallState, ErrorString, MessageString, Status
from utils import default_notifications_cb
from lib import Lib
from call import CallCallback, start_call_sound_out, transmit_recording
from settings import MediaDefaults
import time
from events import AccountEvent,LibEvent

class AccountCallback(pjAccountCallback):
        """
        Class to receive notifications on account's events.
        """
        def __init__(self, logger, account, notification_cb = default_notifications_cb):
            """
            account         : the Account object to which the call back is attached
            logger          : the logger object which has to be used for logging
            notification_cb : callback function to notify about events
            """
            pjAccountCallback.__init__(self, account)
            self.logger = logger
            self.notification_cb = notification_cb
            self.already_registered = False

        def set_account(self, account):
            self._set_account(account)

        def on_reg_state(self):
            """
            on_reg_state
            This is a call back function that will be called by the library notifing
            account registration changes
            """
            account_info = self.account.info()
            reg_status = account_info.reg_status
            reg_reason = account_info.reg_reason
            reg_is_active = account_info.reg_active and account_info.reg_expires > 0

            if reg_status is StatusCode.OK:
                if reg_is_active is False:
                    self.already_registered = False
                    self.account.state = AccountState.UNREGISTERED
                    self.notification_cb(AccountEvent,AccountEvent.ACCOUNT_UNREGISTERED, {'User' : self.account.user_name,'Server': self.account.server, 'reg_status' : reg_status})
                elif self.already_registered is False:
                    self.account.state = AccountState.REGISTERED
                    self.notification_cb(AccountEvent, AccountEvent.ACCOUNT_REGISTERED, {'User' : self.account.user_name, 'Server': self.account.server, 'reg_status' : reg_status })
                    self.already_registered = True
            elif reg_status in [StatusCode.REQUEST_TIMEOUT, StatusCode.SERVICE_UNAVAILABLE]:
                self.account.state = AccountState.UNREGISTERED
                self.notification_cb(LibEvent, LibEvent.LIB_CONNECTION_FAILED, {'User' : self.account.user_name, 'Success' : False, 'status_text' : Status[reg_status], 'reg_reason' : reg_reason})
                self.already_registered = False
            else:
                if reg_is_active is False:
                    self.notification_cb(AccountEvent, AccountEvent.ACCOUNT_REGISTRATION_FAILED,{'User' : self.account.user_name, 'Server': self.account.server, 'reg_status' : reg_status, 'reg_reason' : reg_reason})
                else:
                    self.notification_cb(AccountEvent, AccountEvent.ACCOUNT_UNREGISTRATION_FAILED,{'User' : self.account.user_name, 'Server': self.account.server, 'reg_status' : reg_status, 'reg_reason' : reg_reason})

        def on_incoming_call(self, call):
            """
            when an incoming call comes to the account its called by the library
            call :
            """
            if self.account.current_call is not None:
                self.logger.debug('Already on call... Rejecting with a busy response')
                call.hangup()
                return
            call_cb = CallCallback(call, self.logger, self.account, self.notification_cb)
            call.set_callback(call_cb)
            self.account.current_call = call
            self.account.call_state = CallState[2]
            try:
               player_id = start_call_sound_out(self.account.volume)
               call_cb.set_player_id(player_id)
               time.sleep(1)
            except Exception,e :
                raise e
            self.notification_cb(AccountEvent,AccountEvent.INCOMING_CALL, {'User' : self.account.user_name, 'From' : call.info().remote_uri})
            

class Account(object):
        def __init__(self, user_name, password, sip_server, logger, notification_cb = default_notifications_cb):
            self.server = sip_server
            self.user_name = user_name
            self.logger = logger
            self.password = password
            self.notification_cb = notification_cb

            #initilize the member variables with default values
            self._account = None
            self.current_call = None
            self.call_state = CallState[0]
            self.volume = MediaDefaults.VOLUME
            self.state = AccountState.UNREGISTERED

        def __getattr__(self, name):
           if hasattr(self._account, name):
               return getattr(self._account,name)
           raise Exception("Undefined class member %s" % name)

        def set_volume(self, volume):
           if volume > 1 or volume < 0:
               return False
           else:
               self.volume = volume

        def set_account(self, account):
            """
            set account is called with account of pjsua.Account type and that is
            set as _account in it and all the function  calls to Account will be tried on
            _account if not found in Account class
            """
            self._account = account

        def get_uri(self):
            return self.info().uri

        def register(self, transport = "udp", reg_timeout = 60 , publish_enabled = True):
           """
           Register the account with username and password as provided
           transport       : udp or tcp
           reg_timeout     : sip registration refresh intervel in seconds
           publish_enabled : specify if PUBLISH should be used. When
                           enabled, the PUBLISH will be sent to the
                           registrar.
           returns None on Failure otherwise self
           """
           lib = Lib.instance()
           if lib is None:
               self.logger.debug(ErrorString.UNINITIALIZED_LIB)
               return
           if self.state == AccountState.REGISTERED:
               self.logger.debug(ErrorString.ALREADYREGISTERED)
               return self
           self.logger.debug(MessageString.REGISTERING % (self.user_name, self.password, self.server))
           try:
               if transport is "udp":
                   transport_info = ""
               else:
                   transport_info =  ";transport=tcp"
               acc_cfg = AccountConfig('%s%s' % (str(self.server), transport_info), str(self.user_name), str(self.password))
               acc_cfg.reg_timeout = reg_timeout
               acc_cfg.publish_enabled = publish_enabled
               accountCallback = AccountCallback(self.logger, None, self.notification_cb)
               acc = lib.create_account(acc_config = acc_cfg, cb = accountCallback)
               self.set_account(acc)
               accountCallback.set_account(self)
               return self
           except pjError, e:
               self.logger.exception(ErrorString.REGISTRATION_EXCEPTION % str(e))
               lib.destroy()
               lib = None
               return None

        def play_recording(self, filename):
            if self.current_call is not  None:
                player_id = transmit_recording(self.current_call, filename)
                if player_id is not None:
                    self.current_call._cb.set_player(player_id)
            pass


        def unregister(self):
            """
            unregisters the account if already registered
            """
            if self.state == AccountState.UNREGISTERED:
                return True
            lib = Lib.instance()
            if lib is None:
                self.logger.debug(ErrorString.UNINITIALIZED_LIB)
                return False
            self.logger.debug(MessageString.UNREGISTERING % (self.user_name, self.server))
            self._account.set_registration(False)

        def make_call(self, number = None):
            """
            Makes call to the specified number on the server to which its registered
            number : sip number string like '09742033616'
            """
            lib = Lib.instance()
            if lib is None:
                self.logger.debug(ErrorString.UNINITIALIZED_LIB)
                return False
            if number is None:
                self.logger.debug(ErrorString.MANDATORYARGUEMENTS % ('number', 'make_call'))
                return False
            uri = 'sip:%s@%s' % (str(number), str(self.server))
            lock = ""
            try:
                self.logger.debug(MessageString.CALLING % uri)
                lock = lib.auto_lock()
                cb = CallCallback(None, self.logger, self, self.notification_cb)
                self.current_call = self._account.make_call(uri, cb = cb)
            except pjError, e:
                self.logger.exception(ErrorString.MAKECALL_EXCEPTION, str(e))
                return False
            finally:
                del lock
            try:
                player_id = start_call_sound_out()
                cb.set_player_id(player_id)
            except Exception, e:
                self.logger.exception(ErrorString.UNABLETOSTARTSOUND, str(e))
            return self.current_call

        def hung_up(self):
            if self.current_call is not None:
                try:
                    self.current_call.hangup(200,"LOCAL_HANGUP")
                except Exception:
                    raise Exception("Exception on : dtmf : {0}".format(value))
            else:
                self.logger.debug("Already hungup the call")
        
        def dtmf(self, value):
            if self.current_call is not None:
                try:
                    self.current_call.dial_dtmf(value)
                except Exception,e:
                    raise e
            else:
                raise Exception("Exception on : dtmf : {0}".format(value))

        def __del__(self):
            try:
                self.unregister()
            except Exception:
                pass
