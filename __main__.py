import os
import utils
import events
import lib
import time
from account import Account
import optparse
from threading import RLock

lock = RLock()

def parse_arguements():
    parser = optparse.OptionParser("usage: %prog [options] arg1 arg2")
    parser.add_option("-u", "--user_name", dest="user_name", default="08050103381", type="string", help = "specify the username which ius the sip usernaem to login the client")
    parser.add_option("-s", "--server", dest="host", default='192.168.168.54', type="string", help="hosyt address in which freeswitch is running")
    parser.add_option("-p", "--password", dest="password", default='1234', type="string", help="user password")
    parser.add_option("-a", "--actions", dest="actions", default='1234',  type="string", help="actions file path")
    (options, reminder) = parser.parse_args()
    return options

class NotificationCallback(object):
    def __init__(self):
        self.wait_for_event = None
        self.wait_for_event_name = None
        self._wait = False
    
    def waiting(self):
        return self._wait

    def waitfor(self,event_type,event_name):
        lock.acquire()
        self.wait_for_event = event_type        
        self.wait_for_event_name = event_name
        self._wait = True
        lock.release()
        while self._wait is True:
            time.sleep(1)
        self._wait = False
        
    def __call__(self,event_type, event_name, data):
        @utils.notification_wrapper
        def _f(event_type, event_name, data):
            lock.acquire()
            if self._wait and event_type is self.wait_for_event and  event_name is self.wait_for_event_name:
                self._wait = False
            lock.release()
        return _f(event_type, event_name, data)
            
    pass
class SipAction(object):
    def __init__(self,_ = None):
        pass
    def run(self,_):
        print "default sip run called by ",self.__class__
        pass

class Sleep(SipAction):
    def __init__(self,period):
        self.period = float(period)
        SipAction.__init__(self)
    def run(self,account):
        print "sleep : ({0})".format(self.period)
        time.sleep(self.period)

class Dtmf(SipAction):
    def __init__(self,value):
        self.value = value
        SipAction.__init__(self)
    
    def run(self,account):
        print "dtmf({0})".format(self.value)
        account.dtmf(self.value)
    
class Hangup(SipAction):
    def __init__(self,hangup = True):
        self.hangup = hangup
        SipAction.__init__(self)
    def run(self,account):
        if self.hangup is True:
            account.hangup()

class Skip(SipAction):
    pass


class SipActionFactory(object):
    action_constructors = {'Sleep' : Sleep, 'Dtmf' : Dtmf, 'hangup' : Hangup}
    def __init__(self,logger):
        self._logger = logger
   
    def action(self, i):
        value = None
        action_name_and_value = map(lambda x:x.strip(), i.split('='))
        action_name =  action_name_and_value[0]
        if len(action_name_and_value) == 2:
            value = action_name_and_value[1]
        action_class = SipActionFactory.action_constructors.get(action_name,Skip)
        action = action_class(value)
        return action
           
def action_lines(actions_file):
    logger = utils.getLogger(package = "main")
    if os.path.isfile(actions_file) is False:
        logger.error("file for actions not found")    
    else:
        _actions = []
        with open(actions_file) as actions_f:
            _actions = map(lambda x:x.strip(), actions_f.readlines()[1:])
        for i in _actions:
            print i
            yield i

if __name__ == '__main__':
    options = parse_arguements()
    lib = lib.Lib(utils.getLogger())
    logger = utils.getLogger()
    nc = NotificationCallback()
    a =  Account(options.user_name,options.password, options.host, logger,nc).register()
    actionFactory = SipActionFactory(logger)
    for action_line in action_lines(options.actions):
        print 'WORKING ON SCTION LINE',action_line 
        nc.waitfor(events.AccountEvent,events.AccountEvent.INCOMING_CALL)
        a.current_call.answer(200)
        nc.waitfor(events.CallEvent, events.CallEvent.CALL_ACTIVE)
        for i in action_line.split(','):
            print i
            actionFactory.action(i).run(a)
        nc.waitfor(events.CallEvent, events.CallEvent.CALL_HUNGUP)