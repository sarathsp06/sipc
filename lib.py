#!/usr/bin/python
from pjsua import Lib as pjsuaLib
from pjsua import UAConfig, LogConfig, MediaConfig, TransportType
import settings 
from utils import default_notifications_cb

def log_cb(level, str, len):
    print level, str, len

class Lib(pjsuaLib):
    def __init__(self, logger, notification_cb = default_notifications_cb):
        self.logger = logger
        self.notification_cb = notification_cb
        self.lib = Lib.instance()
        if self.lib is None:
            self.lib = self.__init_pjsualib()

    def __del__(self):
        del(self.lib)

    def __init_pjsualib(self):
        ##User Agent configuration
        ua_cfg = UAConfig()
        ua_cfg.max_calls = settings.MAX_CALLS
        ##Media configuration
        media_cfg = MediaConfig()
        media_cfg.jb_max = settings.MediaDefaults.JITTER_BUFFER_SIZE  #specify the maximum jitter buffer size in
        media_cfg.no_vad = True
        media_cfg.quality = settings.MediaDefaults.AUDIO_QUALITY
        
        ##Log configuration
        log_cfg = LogConfig(level = settings.LOG_LEVEL, callback=log_cb)
        self.lib = pjsuaLib()
        self.lib.init(ua_cfg = ua_cfg, log_cfg = log_cfg, media_cfg = media_cfg)
        transport = self.lib.create_transport(TransportType.UDP)
        self.logger.debug("Creating transport : %s" % transport)
        self.lib.start(with_thread = True)
        return self.lib
    
