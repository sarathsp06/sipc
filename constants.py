#!/usr/bin/python
Status = { 408 : 'REQUEST_TIMEOUT',    #You are screwed
           403 : 'FORBIDDEN',          #still you are screwed
           404 : 'NOT_FOUND',          #still :( 
           200 : 'OK',                 #Cool
           503 : 'SERVICE_UNAVAILABLE' #we are screwed,try checking freeswitch log or call niranjan/govind
         }
MediaState = ['NULL', 'ACTIVE', 'LOCAL_HOLD', 'REMOTE_HOLD', 'ERROR']
"""
    NULL        -- media is not available.
    ACTIVE      -- media is active.
    LOCAL_HOLD  -- media is put on-hold by local party.
    REMOTE_HOLD -- media is put on-hold by remote party.
    ERROR       -- media error (e.g. ICE negotiation failure).
"""
CallState = ['NULL', 'CALLING', 'INCOMING', 'EARLY', 'CONNECTING', 'CONFIRMED', 'DISCONNECTED']               
"""
    Call state constants.
    NULL         -- call is not initialized.
    CALLING      -- initial INVITE is sent.
    INCOMING     -- initial INVITE is received.
    EARLY        -- provisional response has been sent or received.
    CONNECTING   -- 200/OK response has been sent or received.
    CONFIRMED    -- ACK has been sent or received.
    DISCONNECTED -- call is disconnected.
 """

class StatusCode:
    REQUEST_TIMEOUT = 408
    FORBIDDEN = 403
    NOT_FOUND = 404
    OK = 200
    SERVICE_UNAVAILABLE = 503


class AccountState:
    UNREGISTERED = "UNREGISTERED"
    REGISTERED = "REGISTERED"

class ErrorString:
    ALREADYREGISTERED = "The account is already registered so is not attempting registration"
    UNINITIALIZED_LIB = "Lib is not initialized it has to be initalized once to use the application"
    REGISTRATION_EXCEPTION = "\n\nException registering account:%s\n\n" 
    MANDATORYARGUEMENTS = "Mandatory arguments %s not provided for %s" 
    MAKECALL_EXCEPTION = "Exception in make_call : %s"
    UNABLETOSTARTSOUND = "UNABLE TO START SOUND : %s"

class MessageString:
    REGISTERING = 'Registering account with %r, %r, %r'
    UNREGISTERING = 'Un-Registering account with %r, %r'
    CALLING = 'Making call to : %s'


