Usage
---------
python -m sipc -u username -p password -s ip -a inputcsvpath


Sample
----------
python -m sipc -s 192.168.168.54 -p 1234 -u 08050103381 -a sipc/sample_sip.csv



CSV input file format
-----------------------
1. firstline headers
2. following lines comma seperated commands in format command_name=value,command_name1=value1,...
3. invalid commnds will be skipped


AllowedCommands
----------------------
1. Sleep
  * period in milliseconds as value
2. Dtmf
  * digits as value
3. hangup  
