# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 10:40:37 2016

@author: sarath
"""
class _AccountEvent:
    __str__ = lambda self: "ACCOUNT_EVENT"
    ACCOUNT_UNREGISTERED = 'ACCOUNT_UNREGISTERED'
    ACCOUNT_REGISTERED = 'ACCOUNT_REGISTERED'
    ACCOUNT_REGISTRATION_FAILED = 'ACCOUNT_REGISTRATION_FAILED'
    ACCOUNT_UNREGISTRATION_FAILED = 'ACCOUNT_UNREGISTRATION_FAILED'
    INCOMING_CALL = "INCOMINGCALL"
    pass

class _CallEvent:
    __str__ = lambda self: "ACCOUNT_EVENT"
    CALL_HUNGUP = 'CALL_HUNGUP'
    CALL_DIALING = 'CALL_DIALING'
    CALL_ACTIVE = 'CALL_ACTIVE'

    MEDIA_STATE_ACTIVE = 'MEDIA_STATE_ACTIVE'
    MEDIA_STATE_HOLD = 'MEDIA_STATE_HOLD'
    MEDIA_STATE_IDLE = 'MEDIA_STATE_IDLE'
    pass




class _LibEvent:
    __str__ = lambda self: "LIB_EVENT"
    LIB_CONNECTION_FAILED = 'LIB_CONNECTION_FAILED'
    pass


AccountEvent = _AccountEvent()
CallEvent = _CallEvent()
LibEvent = _LibEvent()